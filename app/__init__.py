from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app,db)
login = LoginManager(app)
login.login_view = 'account.login'

from app.event import bp as bp_event
app.register_blueprint(bp_event)

from app.account import bp as bp_account
app.register_blueprint(bp_account)

@app.route('/',methods=['GET','POST'])
@app.route('/index',methods=['GET','POST'])
def index():
    from datetime import date
    from sqlalchemy import func
    from flask import render_template, redirect
    from flask_login import current_user
    from app.models import Event, EventInvolved
    from app.forms import EventInvolveForm

    today = date.today()
    form = EventInvolveForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            mode = form.mode.data
            event_id = form.event_id.data
            account_id = current_user.id
            if mode==0:
                data = EventInvolved(event_id=event_id,account_id=account_id)
                db.session.add(data)
            elif mode==1:
                data = EventInvolved.query.filter_by(event_id=event_id,account_id=account_id).first()
                db.session.delete(data)
            db.session.commit()
        return redirect('/')
    events = db.session.query(Event.id,Event.shortname,Event.start,Event.location,Event.preview,EventInvolved.account_id).join(EventInvolved,isouter=True).filter(func.date(Event.start)>=today).all()
    fields = ['id','shortname','start','location','preview','account_id']
    data = [dict(zip(fields,item)) for item in events]
    return render_template('index.html',data=data,form=form)