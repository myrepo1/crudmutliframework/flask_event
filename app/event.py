from flask import Blueprint, render_template, redirect, url_for, send_from_directory, request
from flask_wtf import FlaskForm
from flask_login import login_required, current_user
from werkzeug.utils import secure_filename
from os import path
import uuid

bp = Blueprint('event',__name__,url_prefix='/event')

from app import db, app
from app.forms import EventForm, DeleteEventForm, EventInvolveForm
from app.models import Event,EventInvolved
from app.files import generate_media_dir, get_media_dir, generate_unique_name, remove_file
from app.account import is_authenticated_user

@bp.route('/add',methods=['GET','POST'])
@login_required
def add_event():
    if not is_authenticated_user():
        return redirect(url_for('account.login'))
    form = EventForm()
    if form.validate_on_submit():
        data = form.get_data()
        preview_file = form.preview.data
        if preview_file:
            preview_name = secure_filename(preview_file.filename)
            preview_path = generate_media_dir('preview')
            preview_name = generate_unique_name(preview_path,preview_name)
            preview_file.save(path.join(preview_path,preview_name))
            data['preview'] = 'preview/'+preview_name
        event = Event(**data)
        db.session.add(event)
        db.session.commit()
        return redirect('/')
    return render_template('event/event_form.html',form=form)

@bp.route('/')
def get_events():
    from datetime import date
    form = EventInvolveForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            mode = form.mode.data
            event_id = form.event_id.data
            account_id = current_user.id
            if mode==0:
                data = EventInvolved(event_id=event_id,account_id=account_id)
                db.session.add(data)
            elif mode==1:
                data = EventInvolved.query.filter_by(event_id=event_id,account_id=account_id).first()
                db.session.delete(data)
            db.session.commit()
        return redirect('/')
    events = db.session.query(Event.id,Event.shortname,Event.start,Event.location,Event.preview,EventInvolved.account_id).join(EventInvolved,isouter=True).all()
    fields = ['id','shortname','start','location','preview','account_id']
    data = [dict(zip(fields,item)) for item in events]
    today = date.today()
    return render_template('index.html',data=data,today=today)

@bp.route('/<int:id>')
def get_event(id):
    data = Event.query.filter_by(id=id).first_or_404()
    involved_account = data.get_involved_account()
    print(dir(involved_account))
    print(involved_account.__len__())
    return render_template('event/event_detail.html',data=data,involved_account=involved_account)

@bp.route('/<int:id>/preview')
def get_event_image(id):
    record = Event.query.filter_by(id=id).first_or_404()
    preview_url = record.preview
    media_dir,filename = preview_url.split('/')
    full_url = get_media_dir(media_dir)
    if path.exists(path.join(full_url,filename)):
        return send_from_directory(full_url,filename)

@bp.route('/<int:id>/edit',methods=['GET','POST'])
def edit_event(id):
    if not is_authenticated_user():
        return redirect(url_for('account.login'))
    form = EventForm(initial_id=id)
    if form.validate_on_submit():
        data = form.get_data()
        record = form.instance
        preview_file = form.preview.data
        if preview_file:
            preview_name = secure_filename(preview_file.filename)
            preview_path = generate_media_dir('preview')
            preview_name = generate_unique_name(preview_path,preview_name)
            preview_file.save(path.join(preview_path,preview_name))
            if record.preview:
                remove_file(record.preview)
            data['preview'] = 'preview/'+preview_name
        for k,v in data.items():
            setattr(record,k,v)
        db.session.commit()
        return redirect('/')
    return render_template('event/event_form.html',form=form)

@bp.route('/<int:id>/delete',methods=['GET','POST'])
def delete_event(id):
    if not is_authenticated_user():
        return redirect(url_for('account.login'))
    form = FlaskForm()
    data = Event.query.filter_by(id=id).first_or_404()
    if form.validate_on_submit():
        db.session.delete(data)
        db.session.commit()
        return redirect('/')
    else:
        return render_template('event/event_delete.html',data=data,form=form)
