from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import StringField,DateTimeField, TextAreaField, SubmitField, PasswordField, IntegerField
from wtforms.validators import DataRequired,Length, ValidationError, Optional, EqualTo, InputRequired
from wtforms.widgets import TextInput, HiddenInput

from app.models import Event, Account

class MyDateInput(TextInput):
    def __call__(self,field,**kwargs):
        c = kwargs.pop('class','') or kwargs.pop('class_','')
        c = c + ' date-time-picker'
        kwargs['class'] = c
        return super().__call__(field,**kwargs)


class StandardModelForm(FlaskForm):
    def __init__(self, model,*args, instance_id=None, **kwargs):
        super().__init__(*args,**kwargs)
        self.form_errors = []
        self.model = model
        if instance_id:
            self.instance = self.model.query.filter_by(id=instance_id).first()
            if not self.instance:
                raise ValidationError('Instance model not found.')
            self.populate_initial_field()
        else:
            self.instance = None

    def populate_initial_field(self):
        for k,v in self.instance.__dict__.items():
            if k in self._fields and not self._fields[k].data:
                self._fields[k].data = v

    def is_change(self,field,value):
        if self.instance:
            return getattr(self.instance,field) != value
        else:
            return True

    def check_exist(self,field_name,field):
        if self.is_change(field_name,field.data):
            params = {}
            params[field_name] = field.data
            record = self.model.query.filter_by(**params).first()
            if record is not None:
                return True
        return False

    def get_data(self):
        result = {}
        for name,field in self._fields.items():
            if name == 'csrf_token':
                continue
            elif isinstance(field,FileField):
                continue
            result[name] = field.data
        return result


class EventForm(StandardModelForm):
    name = StringField('Name',validators=[DataRequired(),Length(min=1,max=150)])
    shortname = StringField('Shortname',validators=[Length(max=15)])
    start = DateTimeField('Start',validators=[DataRequired()],format='%d-%m-%Y %H:%M', widget=MyDateInput())
    end = DateTimeField('End',format='%d-%m-%Y %H:%M', widget=MyDateInput(), validators=[Optional(strip_whitespace=True)])
    description = TextAreaField('Description',validators=[Length(max=1000)])
    location = StringField('Location',validators=[Length(max=150)])
    preview = FileField('Preview')

    def __init__(self,*args,**kwargs):
        super().__init__(Event,*args,**kwargs)

    def validate_name(self,name):
        if self.check_exist('name',name):
            raise ValidationError(f'Event with name {name.data} already exist.')
        else:
            return True

    def validate_startend(self):
        start = self.start.data
        end = self.end.data
        if start and end and start > end:
            self.form_errors.append('Start must be before end.')
            return False
        return True

    def validate(self,*args,**kwargs):
        result = super().validate(*args,**kwargs)
        return result and self.validate_startend()

class DeleteEventForm(FlaskForm):
    submit = SubmitField('Delete')


class LoginForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired()])
    password = PasswordField('Password',validators=[DataRequired()])

    def __init__(self,*args, **kwargs):
        super().__init__(*args,**kwargs)
        self.form_errors = []
        self.account = None

    def validate_login(self):
        username = self._fields['username'].data
        password = self._fields['password'].data
        account = Account.query.filter_by(username=username).first()
        if not (account and account.check_password(password)):
            self.form_errors.append('Wrong username or password.')
            return False
        else:
            self.account = account
            return True

    def validate(self,*args,**kwargs):
        result = super().validate(*args,**kwargs)
        return result and self.validate_login()

class AccountForm(StandardModelForm):
    username = StringField('Username',validators=[DataRequired()])
    password = PasswordField('Password')
    password1 = PasswordField('Repeat Password',validators=[EqualTo('password',message='Password not match')])
    name = StringField('Fullname',validators=[DataRequired()])
    phone = StringField('Phone')
    profpic = FileField('Profpic')

    def __init__(self,*args,**kwargs):
        super().__init__(Account,*args,**kwargs)
        if self.instance:
            self._fields['password1'].validators.append(Optional())
        else:
            self._fields['password'].validators.append(DataRequired())
            self._fields['password1'].validators.append(DataRequired())

    def validate_username(self,username):
        if self.check_exist('username',username):
            raise ValidationError(f'Account with username {username} already exists.')

    def validate_phone(self,phone):
        if not phone.data.isnumeric():
            raise ValidationError(f'Phone {phone.data} is not a valid phone number.')


class EventInvolveForm(FlaskForm):
    mode = IntegerField(widget=HiddenInput(),validators=[InputRequired()])
    event_id = IntegerField(widget=HiddenInput(),validators=[InputRequired()])

    def validate_mode(self,mode):
        data = mode.data
        if data in [0,1]:
            return True
        else:
            raise ValidationError('Only 0 or 1 allowed in mode')

    def validate_event_id(self,event_id):
        data = event_id.data
        record = Event.query.get(data)
        if record:
            return True
        else:
            raise ValidationError(f'Record with id {data} not found.')
