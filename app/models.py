from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash
from app import db, login

class Event(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(150))
    shortname = db.Column(db.String(15))
    start = db.Column(db.DateTime,default=datetime.utcnow)
    end = db.Column(db.DateTime)
    description =  db.Column(db.Text(1000))
    location = db.Column(db.String(150))
    preview = db.Column(db.String(200))
    involved = db.relationship('EventInvolved',backref='event',lazy=True)

    def get_involved_account(self):
        data = db.session.query(Account.id,Account.name,Account.phone,EventInvolved.event_id).join(EventInvolved).filter(EventInvolved.event_id==self.id)
        columns = ['id','name','phone','event_id']
        if data:
            formated_data = [dict(zip(columns,item)) for item in data]
            return formated_data
        else:
            return None

    def __repr__(self):
        name = self.shortname if self.shortname else self.name
        return f'<Event {name}>'

class Account(UserMixin,db.Model):
    id = db.Column(db.Integer,primary_key=True)
    username = db.Column(db.String(150))
    password = db.Column(db.String(150))
    name = db.Column(db.String(150))
    phone = db.Column(db.String(20))
    is_staff = db.Column(db.Boolean,default=False)
    profpic = db.Column(db.String(200))
    involved = db.relationship('EventInvolved',backref='account',lazy=True)

    def __repr__(self):
        return f'<User {self.username}>'

    def set_password(self,value):
        self.password = generate_password_hash(value)

    def check_password(self,value):
        return check_password_hash(self.password,value)

    def get_event_involved(self):
        data = db.session.query(Event.id,Event.shortname,Event.start,Event.location,EventInvolved.account_id).join(EventInvolved).filter(EventInvolved.account_id==self.id)
        columns = ['id','shortname','start','location','account_id']
        if data:
            formated_data = [dict(zip(columns,item)) for item in data]
            return formated_data
        else:
            return None

class EventInvolved(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    event_id = db.Column(db.Integer,db.ForeignKey('event.id'))
    account_id = db.Column(db.Integer,db.ForeignKey('account.id'))


@login.user_loader
def load_user(id):
    return Account.query.get(int(id))