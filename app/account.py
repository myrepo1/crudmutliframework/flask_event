from flask import Blueprint,render_template, redirect, request, url_for, send_from_directory
from flask_login import current_user, login_user, logout_user, login_required
from flask_wtf import FlaskForm
from os import path
from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename

from app.forms import LoginForm, AccountForm
from app.models import Account
from app.files import generate_media_dir, generate_unique_name, get_media_dir, remove_file
from app import db

bp = Blueprint('account',__name__,url_prefix='/account')

def is_authenticated_user(id=None):
    if current_user.is_staff:
        return True
    elif id and current_user.id == id:
        return True
    else:
        return False

@bp.route('/login',methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    if form.validate_on_submit():
        login_user(form.account)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    else:
        return render_template('account/login.html',form=form)

@bp.route('/logout')
def logout():
    logout_user()
    return redirect('/')

@bp.route('/add',methods=['GET','POST'])
def add_account():
    if current_user.is_authenticated:
        return redirect('/')
    if not is_authenticated_user():
        return redirect(url_for('account.login'))
    form = AccountForm()
    if form.validate_on_submit():
        data = form.get_data()
        profpic_file = form.profpic.data
        if profpic_file:
            profpic_name = secure_filename(profpic_file.filename)
            profpic_path = generate_media_dir('profpic')
            profpic_name = generate_unique_name(profpic_path,profpic_name)
            profpic_file.save(path.join(profpic_path,profpic_name))
            data['profpic'] = 'profpic/'+profpic_name
        account = Account(**data)
        db.session.add(account)
        db.session.commit()
        return redirect(url_for('account.login'))
    else:
        return render_template('account/account_form.html',form=form)

@bp.route('<int:id>')
@login_required
def get_account(id):
    if not is_authenticated_user(id=id):
        return redirect(url_for('account.login'))
    account = Account.query.filter_by(id=id).first_or_404()
    involved_event = account.get_event_involved()
    return render_template('account/account_detail.html',data=account,involved_event=involved_event)

@bp.route('/<int:id>/profpic')
def get_account_image(id):
    if not is_authenticated_user(id=id):
        return redirect(url_for('account.login'))
    record = Account.query.filter_by(id=id).first_or_404()
    profpic_url = record.profpic
    media_dir,filename = profpic_url.split('/')
    full_url = get_media_dir(media_dir)
    if path.exists(path.join(full_url,filename)):
        return send_from_directory(full_url,filename)

@bp.route('<int:id>/edit',methods=['GET','POST'])
def edit_account(id):
    if not is_authenticated_user(id=id):
        return redirect(url_for('account.login'))
    form = AccountForm(instance_id=id)
    if form.validate_on_submit():
        record = form.instance
        data = form.get_data()
        profpic_file = form.profpic.data
        if profpic_file:
            profpic_name = secure_filename(profpic_file.filename)
            profpic_path = generate_media_dir('profpic')
            profpic_name = generate_unique_name(profpic_path,profpic_name)
            profpic_file.save(path.join(profpic_path,profpic_name))
            if record.profpic:
                remove_file(record.profpic)
            data['profpic'] = 'profpic/'+profpic_name
        for k,v in data.items():
            setattr(record,k,v)
        db.session.commit()
        return redirect('/')
    else:
        return render_template('account/account_form.html',form=form)

@bp.route('<int:id>/delete',methods=['GET','POST'])
def delete_account(id):
    if not is_authenticated_user(id=id):
        return redirect(url_for('account.login'))
    form = FlaskForm()
    data = Account.query.filter_by(id=id).first_or_404()
    if form.validate_on_submit():
        db.session.delete(data)
        db.commit()
        return redirect('/')
    else:
        return render_template('account/account_delete.html',form=form,data=data)