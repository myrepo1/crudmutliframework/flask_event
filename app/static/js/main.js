function submitInvolvement() {
    const button = document.activeElement;
    const mode = parseInt(button.getAttribute('mode'));
    const event_id = parseInt(button.getAttribute('event'));
    
    const mode_input = document.getElementById('mode');
    const event_input = document.getElementById('event_id');
    mode_input.value = mode;
    event_input.value = event_id;
    document.getElementById('eventInvolvementForm').submit();
}