from os import path, makedirs, remove
from config import Config
import uuid

from app import app

def generate_media_dir(file_dir):
    complete_path = get_media_dir(file_dir)
    if not path.exists(complete_path):
        makedirs(complete_path)
    return complete_path

def get_media_dir(file_dir):
    media_dir = Config.MEDIA_DIR
    return path.join(app.root_path,media_dir,file_dir)

def generate_unique_name(file_path,name):
    if path.exists(path.join(file_path,name)):
        initial_name,ext = name.split('.')
        return initial_name + uuid.uuid4().hex +'.'+ ext
    else:
        return name

def remove_file(file_dir):
    media_dir = Config.MEDIA_DIR
    full_path = path.join(app.root_path,media_dir,file_dir)
    try:
        remove(full_path)
    except:
        return