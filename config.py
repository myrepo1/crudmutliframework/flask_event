import os
BASEDIR = os.path.join(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = 'askdjaskdlasjdlkasjdkalsd'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///'+os.path.join(BASEDIR,'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MEDIA_DIR = 'media'