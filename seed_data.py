from datetime import datetime,timedelta
from config import Config
from os import path, makedirs
from shutil import rmtree, copyfile
from uuid import uuid4
from werkzeug.security import generate_password_hash

from app.models import Event, Account, EventInvolved
from app import db

now = datetime.now()

events = [
    {
        'name':'Bazaar Rame Tahunan',
        'shortname':'bazaar20',
        'start':now+timedelta(days=5),
        'end':now+timedelta(days=8),
        'description':'Event bazaar tahunan. Banyak jualan disini.',
        'location':'Gedung Sate'
        },
    {
        'name':'Theater Perjuangan Kemerdekaan 1945',
        'shortname':'TP45',
        'start':now+timedelta(days=4),
        'end':now+timedelta(days=4,hours=2),
        'description':'Pertunjukan proses perjuangan kemerdekaan.',
        'location':'Gedung Merdeka'
        },
    {
        'name':'Lomba lari marathon se Bandung',
        'shortname':'maraBDG',
        'start':now+timedelta(days=-5),
        'end':now+timedelta(days=-5,hours=5),
        'description':'Lomba lari marathon se bandung.',
        'location':'Jalan Merdeka'
    },
    {
        'name':'Layar Tancap Bandung Tempo Doeloe',
        'shortname':'oldBDG',
        'start':now+timedelta(days=3),
        'end':now+timedelta(days=3,hours=3),
        'description':'Nonton bareng gambaran Bandung tempo dulu.',
        'location':'Museum Sri Baduga'
    },
    {
        'name':'1000 Pohon hijaukan Negeri',
        'shortname':'1000hijau',
        'start':now+timedelta(days=10),
        'description':'Menanam pohon bersama untuk penghijauan.',
        'location':'Babakan Siliwangi'
    }
]

accounts = [
    {
        'username':'admin',
        'is_staff':True,
        'password':'admin321_123',
        'name':'Event Admin',
        'phone':'08098912312'
    },
    {
        'username':'person1',
        'password':'person_321',
        'name':'Budi',
        'phone':'12930123213',
    },
]

def create_account(data):
    data['password'] = generate_password_hash(data['password'])
    record = Account(**data)
    db.session.add(record)


def create_event(data):
    record = Event(**data)
    db.session.add(record)

def seed_data():
    print('Begin seed data...')
    makedirs('app/media/preview')
    if not path.exists('seed_data/event.jpg'):
        print('seed_data/event.jpg not found.')
        return
    name = 'event'
    ext = '.jpg'
    for account in accounts:
        create_account(account)
    for event in events:
        filename = name + '_' + uuid4().hex + ext
        full_filename = 'preview/' + filename
        copyfile('seed_data/event.jpg','app/media/preview/'+filename)
        event['preview'] = full_filename
        create_event(event)
    db.session.commit()
    joined_event = Event.query.filter_by(name=events[1]['name']).first()
    joined_account = Account.query.filter_by(username=accounts[1]['username']).first()
    event_involved = EventInvolved(event_id=joined_event.id,account_id=joined_account.id)
    db.session.add(event_involved)
    db.session.commit()
    print('Finish seed data...')

def clear_data():
    db.drop_all()
    print('Finish clear database')
    try:
        rmtree('app/media')
        print('Finish remove file')
    except OSError:
        print('Failed remove file')